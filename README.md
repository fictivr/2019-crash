Unity 2019 or 2018 editor on MacOS crashes if an exception is thrown, sometimes before but always after the Oculus Lipsync DLL/bundle is loaded.

[Unity Bug report](https://fogbugz.unity3d.com/default.asp?1318755_kf5srfl5ptl2gujl)
[Oculus Bug Report](https://developer.oculus.com/bugs/bug/5971339786225024/)

# What happened

Unity keeps crashing on MacOS. It seemed irregular but after investigation, we found that the crash happens when an exception, such as a NullReferenceException, is thrown some time AFTER loading the Oculus Lipsync DLL/bundle. It doesn't matter if the exception is caught or not. If no exception thrown, it seems to work as usual and use the DLL. It also sometimes crashes if the exception is thrown just before loading the DLL, but then it seems to not happen on first Unity play, just subsequent plays. After investigation, we could reproduce it on a fresh install of Unity 2018.4.21, 2019.4.18, 2019.4.20 and 2019.4.21 on two different MacOS, Mojave and Big Sur.

Further investigation showed that this happened on Lipsync releases 20.0, 1.43, 1.42 **but NOT on 1.41**!

# How we can reproduce it

On a Mac, load Unity 2019.4.21, install [Oculus Lipsync](https://developer.oculus.com/downloads/package/oculus-lipsync-unity/20.0.0/) and open the scene Assets/Oculus/LipSync/Scenes/LipSync_Demo.unity (official Oculus Lipsync demo scene). It should crash when pressing Play.

Note that we have made changes in the script Assets/Oculus/LipSync/Scripts/OVRLipSyncContextBase.cs as that is the one the loads the library. 

On line 126 we create an Exception, which will not crash Unity at first run, but will crash if you re-run it again after.
The same Exception generator called on line 143 will crash Unity. 

If we comment out the exceptions all together, the scene runs fine. This shows that exceptions thrown after loading a DLL fails on MacOS. We also see in the Editor.log several lines stating that "Fallback handler could not load library" on a clearly incorrect path, e.g. /Applications/Unity/Hub/Editor/2019.4.21f1/Unity.app/Contents/Frameworks/Mono/lib/libAssets/Oculus/LipSync/Plugins/MacOSX/OVRLipSync.bundle which is the path for the Mono lib concatenated with the relative path of the DLL inside the project (not even separated by a slash). However, we don't have any messages about DllNotFoundException and the DLL has been re-imported so should not be corrupted.

The crash report from MacOS shows a segmentation fault in the thread Profiler.Dispatcher. “A segmentation fault means you are trying to access a memory region that doesn't belong to your app. In your case, it may be cause be auto-released objects that gets deallocated, and then accessed (the pointer is not valid anymore, since the object was deallocated)”

# Theories

It could be related to the unloading of some resource, which happens when an exception is thrown? Or that the DLL is unloaded but then referenced somehow.

# Tested solutions

- Setting the Assets/Oculus/LipSync/Plugins/MacOSX/OVRLipSync.bundle to load at startup: no difference
- Setting the Assets/Oculus/LipSync/Plugins/MacOSX/OVRLipSync.bundle to all platforms: no difference
- Setting the Assets/Oculus/LipSync/Plugins/MacOSX/OVRLipSync.bundle Editor platform to any CPU: no difference
- Deselecting Editor as available platform for Assets/Oculus/LipSync/Plugins/MacOSX/OVRLipSync.bundle: no crash but DLL is not found and lipsync doesn't work
- Building it and launching as standalone executable: crash still happens 
- Change from Mono to IL2CPP: the built standalone executable doesn't crash, but the editor still crashes

# Workaround

As mentioned above, downgrade Lipsync to 1.41 and it works.

# Note

There is an unrelated Exception in the project:
UnassignedReferenceException: The variable SwitchTargets of EnableSwitch has not been assigned.

This comes because we reduced the GameObjects in the scene to absolute minimum and some are still expected by other code.